CONNECT 'jdbc:mariadb://localhost:3306;create=true;user=aleksandra;password=aleksandra';

DROP TABLE author;
DROP TABLE exposition;
DROP TABLE hall;
DROP TABLE picture;
DROP TABLE user;

CREATE TABLE exposition(
id INTEGER NOT NULL PRIMARY KEY,
topic VARCHAR(50) NOT NULL,
date_start DATE,
date_end DATE,
cost INTEGER NOT NULL,
num_of_tickets INTEGER NOT NULL,
hall_id INTEGER NOT NULL REFERENCES hall(id)
);

INSERT INTO exposition VALUES (DEFAULT, 'summer', '2020-11-02', '2020-12-15', '200', '2000' );
INSERT INTO exposition VALUES (DEFAULT, 'winter', '2020-11-10', '2020-12-02', '150', '2000' );
INSERT INTO exposition VALUES (DEFAULT, 'spring', '2020-12-02', '2021-02-15', '300', '2000' );
INSERT INTO exposition VALUES (DEFAULT, 'autumn', '2020-10-25', '2020-12-05', '200', '2000' );


CREATE TABLE hall(
    id   INTEGER     NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

INSERT INTO hall VALUES (DEFAULT, 'red');
INSERT INTO hall VALUES (DEFAULT, 'blue');
INSERT INTO hall VALUES (DEFAULT, 'green');
INSERT INTO hall VALUES (DEFAULT, 'yellow');

SELECT * FROM exposition;
SELECT * FROM hall;

DISCONNECT;