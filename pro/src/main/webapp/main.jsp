<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Exposition</title>
</head>


<body>
<a href="?command=localization&locale=ru">ru</a>/<a href="?command=localization&locale=en">en</a>
<h1>${hello}</h1>
If you want to see available expositions, click here
<a href="?command=expositions">
    list exposition
</a>
<ul>
<c:forEach var="exposition" items="${expositions}">
<c:url value="exposition.jsp" var="url">
    <c:param name="topic" value="${exposition.topic}"/>
    <c:param name="date_start" value="${exposition.dateStart}"/>
    <c:param name="date_end" value="${exposition.dateEnd}"/>
    <c:param name="cost" value="${exposition.cost}"/>
</c:url>
<li><a href="${url}">${exposition.topic}</a></li>
</c:forEach>
</ul>


</body>

</html>


