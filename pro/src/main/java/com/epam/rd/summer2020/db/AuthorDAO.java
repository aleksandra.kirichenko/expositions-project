package com.epam.rd.summer2020.db;

import com.epam.rd.summer2020.db.entity.AuthorDTO;
import com.epam.rd.summer2020.db.entity.EntityMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorDAO {
    private static final String SQL__GET_AUTHOR_BY_PICTURE_ID =
            "SELECT surname, name FROM author WHERE id = ?";

    AuthorMapper mapper = new AuthorMapper();

    public AuthorDTO getAuthorByAuthorId(Long authorId) {
        AuthorDTO authorDTO = new AuthorDTO();
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement stat = con.prepareStatement(SQL__GET_AUTHOR_BY_PICTURE_ID);
            stat.setLong(1, authorId);
            stat.execute();
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    return mapper.mapRow(result);
            }

        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(con);
            e.printStackTrace();
        }
        return null;
    }

    private static class AuthorMapper implements EntityMapper<AuthorDTO> {
        @Override
        public AuthorDTO mapRow(ResultSet rs) {
            try {
                AuthorDTO authorDTO = new AuthorDTO();
                authorDTO.setSurname(rs.getString(Fields.AUTHOR__SURNAME));
                authorDTO.setName(rs.getString(Fields.AUTHOR__NAME));
                return authorDTO;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
