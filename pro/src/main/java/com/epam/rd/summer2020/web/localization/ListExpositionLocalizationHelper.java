package com.epam.rd.summer2020.web.localization;

import com.epam.rd.summer2020.db.LocalizationDAO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class ListExpositionLocalizationHelper extends LocalizationHelper {
    @Override
    public void localize(HttpServletRequest request, String forward) {
        String language = getLanguage(request);
        Map<String, String> values = new LocalizationDAO().getValues(forward, language);
        request.setAttribute("hello", values.get("hello"));
    }
}
