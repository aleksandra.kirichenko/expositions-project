package com.epam.rd.summer2020.web.command;

import com.epam.rd.summer2020.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeLanguageCommand extends Command{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String locale = request.getParameter("locale");
        HttpSession session = request.getSession();
        if (locale.equalsIgnoreCase("RU")) {
            session.setAttribute("locale", "RU");
        }else {
            session.setAttribute("locale", "EN");
        }
        return Path.MAIN_PAGE;
    }
}
