package com.epam.rd.summer2020;

public final class Path {

    public static final String PAGE__ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
    public static final String MAIN_PAGE = "main.jsp";
    public static final String EXPOSITIONS = "exposition.jsp";

}
