package com.epam.rd.summer2020.web.command;

import com.epam.rd.summer2020.Path;
import com.epam.rd.summer2020.db.ExpositionDAO;
import com.epam.rd.summer2020.db.entity.ExpositionDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ListExpositionCommand extends Command{
    private static final Logger log = Logger.getLogger(ListExpositionCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        // get menu items list
        List<ExpositionDTO> expositions = new ExpositionDAO().getAllExpositions();
        log.trace("Found in DB: expositionsList --> " + expositions);

        // put expositions list to the request
        request.setAttribute("expositions", expositions);
        log.trace("Set the request attribute: expositions --> " + expositions);

        log.debug("Command finished");
        return Path.MAIN_PAGE;
    }
}
