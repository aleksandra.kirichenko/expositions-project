package com.epam.rd.summer2020.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class LocalizationDAO {
    private static final String SQL_GET_DATA_MAP = "SELECT name, value from l10n_languages WHERE page = ? and language = ?";

    public Map<String, String> getValues(String path, String locale) {
        HashMap<String, String> ret = new HashMap<>();
        try (Connection con = DBManager.getInstance().getConnection(); PreparedStatement stat = con.prepareStatement(SQL_GET_DATA_MAP)) {
            stat.setString(1, path);
            stat.setString(2, locale);
            try (ResultSet result = stat.executeQuery()) {
                while (result.next()) {
                    ret.put(result.getString(1), result.getString(2));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return ret;
    }
}
