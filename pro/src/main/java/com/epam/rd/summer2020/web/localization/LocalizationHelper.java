package com.epam.rd.summer2020.web.localization;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public abstract class LocalizationHelper {
    public abstract void localize(HttpServletRequest request, String forward);

    protected String getLanguage(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session == null) {
            return "EN";
        }
        Object locale = session.getAttribute("locale");
        return locale == null ? "EN" : String.valueOf(locale);
    }
}


