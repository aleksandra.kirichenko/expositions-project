package com.epam.rd.summer2020.db;

import com.epam.rd.summer2020.db.entity.EntityMapper;
import com.epam.rd.summer2020.db.entity.HallDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HallDAO {

    private static final String SQL__ADD_HALL =
            "INSERT INTO hall name VALUE ?";
    private static final String SQL__GET_HALL =
            "SELECT id, name, hall_exposition_id FROM hall";
    private static final String SQL__GET_HALLByExpositionId = "SELECT hall.id, hall.name\n" +
            "FROM exposition\n" +
            "         JOIN hall_exposition ON exposition.id = hall_exposition.exposiition_id\n" +
            "         JOIN hall  on hall_exposition.hall_id = hall.id\n" +
            "WHERE exposition.id = ?";

    HallMapper mapper = new HallMapper();


    public List<HallDTO> getHallsByExpositionId(long expositionId) {
        List<HallDTO> hallDTOs = new ArrayList<>();
        try (Connection con = DBManager.getInstance().getConnection(); PreparedStatement stat = con.prepareStatement(SQL__GET_HALLByExpositionId)) {
            stat.setLong(1, expositionId);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first()) {
                    HallDTO hallDTO = mapper.mapRow(result);
                    hallDTOs.add(hallDTO);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return hallDTOs;
    }

    public void addHall(HallDTO hallDTO) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement stat = con.prepareStatement(SQL__ADD_HALL);
            stat.setString(1, hallDTO.getName());
            stat.execute();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }


    private static class HallMapper implements EntityMapper<HallDTO> {
        @Override
        public HallDTO mapRow(ResultSet rs) {
            try {
                HallDTO hallDTO = new HallDTO();
                hallDTO.setName(rs.getString(Fields.HALL__NAME));
                return hallDTO;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
