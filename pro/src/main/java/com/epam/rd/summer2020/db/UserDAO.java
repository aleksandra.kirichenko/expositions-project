package com.epam.rd.summer2020.db;

import com.epam.rd.summer2020.db.entity.EntityMapper;
import com.epam.rd.summer2020.db.entity.UserDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO {
    private static final String SQL__ADD_USER =
            "INSERT INTO user (login, password, role_id) VALUES (?,?,?)";
    private static final String SQL__FIND_USER_BY_ID =
            "SELECT login, password, role_id FROM user WHERE id=?";

    public void addUser(UserDTO userDTO) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement stat = con.prepareStatement(SQL__ADD_USER);
            stat.setString(1, userDTO.getLogin());
            stat.setString(2, userDTO.getPassword());
            stat.setInt(3, userDTO.getRoleId());
            stat.execute();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    public UserDTO findUserById(Long id) {
        UserDTO userDTO = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            UserDTOMapper mapper = new UserDTOMapper();
            pstmt = con.prepareStatement(SQL__FIND_USER_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                userDTO = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return userDTO;
    }

    private static class UserDTOMapper implements EntityMapper<UserDTO> {

        @Override
        public UserDTO mapRow(ResultSet rs) {
            try {
                UserDTO user = new UserDTO();
                user.setLogin(rs.getString(Fields.USER__LOGIN));
                user.setPassword(rs.getString(Fields.USER__PASSWORD));
                user.setRoleId(rs.getInt(Fields.USER__ROLE_ID));
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
