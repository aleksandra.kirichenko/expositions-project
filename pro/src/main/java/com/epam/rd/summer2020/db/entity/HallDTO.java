package com.epam.rd.summer2020.db.entity;

public class HallDTO extends Entity {
    String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "HallDTO{" +
                "name='" + name + '\'' +
                '}';
    }
}
