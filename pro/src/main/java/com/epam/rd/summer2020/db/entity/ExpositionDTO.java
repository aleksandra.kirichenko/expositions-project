package com.epam.rd.summer2020.db.entity;

import java.sql.Date;


public class ExpositionDTO extends Entity {
    String topic;
    Date dateStart;
    Date dateEnd;
    int cost;
    int numOfTickets;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getNumOfTickets() {
        return numOfTickets;
    }

    public void setNumOfTickets(int numOfTickets) {
        this.numOfTickets = numOfTickets;
    }

    @Override
    public String toString() {
        return "ExpositionDTO{" +
                "topic='" + topic + '\'' +
                ", dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                ", cost=" + cost +
                ", numOfTickets=" + numOfTickets +
                '}' + System.lineSeparator();
    }
}
