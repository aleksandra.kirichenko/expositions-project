package com.epam.rd.summer2020.db;

import com.epam.rd.summer2020.db.entity.EntityMapper;
import com.epam.rd.summer2020.db.entity.ExpositionDTO;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ExpositionDAO {
    private static final String SQL__GET_ALL_EXPOSITIONS =
            "SELECT id, topic, date_start, date_end, cost, num_of_tickets FROM exposition";
    private static final String SQL__FILTER_BY_DATE =
            " SELECT id, topic, date_start, date_end, cost, num_of_tickets  FROM exposition" +
                    " WHERE date_start BETWEEN  ? AND ?";
    private static final String SQL__ADD_NEW_EXPOSITION =
            "INSERT INTO exposition (topic, date_start, date_end, cost, num_of_tickets)" +
                    " VALUES(?, ?, ?, ?, ?)";
    private static final String SQL__DELETE_EXPOSITION =
            "DELETE FROM exposition WHERE topic = ?";
    private static final String SQL__GET_EXPOSITION_BY_ID =
            "SELECT id, topic, date_start, date_end, cost, num_of_tickets FROM exposition WHERE id=?";

    ExpositionMapper mapper = new ExpositionMapper();

    public List<ExpositionDTO> getAllExpositions() {
        List<ExpositionDTO> expositionDTOS = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL__GET_ALL_EXPOSITIONS);
            while (rs.next())
                expositionDTOS.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return expositionDTOS;
    }

    public ExpositionDTO getExpositionById(long id) {
        try (Connection con = DBManager.getInstance().getConnection(); PreparedStatement stat = con.prepareStatement(SQL__GET_EXPOSITION_BY_ID)) {
            stat.setLong(1, id);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first())
                    return mapper.mapRow(result);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public void addNewExposition(ExpositionDTO expositionDTO) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement stat = con.prepareStatement(SQL__ADD_NEW_EXPOSITION);
            stat.setString(1, expositionDTO.getTopic());
            stat.setDate(2, expositionDTO.getDateStart());
            stat.setDate(3, expositionDTO.getDateEnd());
            stat.setInt(4, expositionDTO.getCost());
            stat.setInt(5, expositionDTO.getNumOfTickets());
            stat.execute();
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(con);
            e.printStackTrace();
        }
    }

    public void deleteExpositinByTopic(String topic) {
        ExpositionDTO expositionDTO = new ExpositionDTO();
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement stat = con.prepareStatement(SQL__DELETE_EXPOSITION);
            stat.setString(1, topic);
            stat.execute();
        } catch (SQLException e) {
            DBManager.getInstance().rollbackAndClose(con);
            e.printStackTrace();
        }
    }

    public List<ExpositionDTO> filterByDate(Date from, Date till) {
        List<ExpositionDTO> expositionDTOs = new ArrayList<>();
        try (Connection con = DBManager.getInstance().getConnection(); PreparedStatement stat = con.prepareStatement(SQL__FILTER_BY_DATE)) {
            stat.setDate(1, from);
            stat.setDate(2, till);
            try (ResultSet result = stat.executeQuery()) {
                while (result.next()) {
                    ExpositionDTO expositionDTO = mapper.mapRow(result);
                    expositionDTOs.add(expositionDTO);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return expositionDTOs;
    }


    private static class ExpositionMapper implements EntityMapper<ExpositionDTO> {

        @Override
        public ExpositionDTO mapRow(ResultSet rs) {
            try {
                ExpositionDTO expositionDTO = new ExpositionDTO();
                expositionDTO.setId(rs.getLong(Fields.ENTITY__ID));
                expositionDTO.setTopic(rs.getString(Fields.EXPOSITION__TOPIC));
                expositionDTO.setDateStart(rs.getDate(Fields.EXPOSITION__DATE_START));
                expositionDTO.setDateEnd(rs.getDate(Fields.EXPOSITION__DATE_END));
                expositionDTO.setCost(rs.getInt(Fields.EXPOSITION__COST));
                expositionDTO.setNumOfTickets(rs.getInt(Fields.EXPOSITION__NUM_OF_TICKETS));
                return expositionDTO;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}