package com.epam.rd.summer2020.web.localization;

import com.epam.rd.summer2020.Path;

import java.util.Map;
import java.util.TreeMap;

public class LocalizationContainer {

    private static Map<String, LocalizationHelper> containers = new TreeMap<>();

    static {
        containers.put(Path.MAIN_PAGE, new ListExpositionLocalizationHelper());
        containers.put(Path.EXPOSITIONS, new ListExpositionLocalizationHelper());
    }

    public static LocalizationHelper get(String forward) {
        return containers.get(forward);
    }
}
