package com.epam.rd.summer2020.db;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DB manager. Works with Apache MariaDB.
 * Only the required DAO methods are defined!
 */
public class DBManager {

    private static Logger log;

    static {
        try {
            log = Logger.getLogger(Class.forName(DBManager.class.getName()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    // //////////////////////////////////////////////////////////
    // singleton
    // //////////////////////////////////////////////////////////

    private static com.epam.rd.summer2020.db.DBManager instance;

    public static synchronized com.epam.rd.summer2020.db.DBManager getInstance() {
        if (instance == null)
            instance = new com.epam.rd.summer2020.db.DBManager();
        return instance;
    }

    /**
     * Returns a DB connection from the Pool Connections. Before using this
     * method you must configure the Date Source and the Connections Pool in your
     * WEB_APP_ROOT/META-INF/context.xml file.
     *
     * @return A DB connection.
     */
//	public Connection getConnection() throws SQLException {
//		Connection con = null;
//		try {
//			Context initContext = new InitialContext();
//			Context envContext  = (Context)initContext.lookup("java:/comp/env");
//
//			// ST4DB - the name of data source
//			DataSource ds = (DataSource)envContext.lookup("jdbc/ST4DB");
//			con = ds.getConnection();
//		} catch (NamingException ex) {
//			log.error("Cannot obtain a connection from the pool", ex);
//		}
//		return con;
//	}
    private DBManager() {
    }


    // //////////////////////////////////////////////////////////
    // DB util methods
    // //////////////////////////////////////////////////////////

    /**
     * Commits and close the given connection.
     *
     * @param con Connection to be committed and closed.
     */
    public void commitAndClose(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Rollbacks and close the given connection.
     *
     * @param con Connection to be rollbacked and closed.
     */
    public void rollbackAndClose(Connection con) {
        try {
            con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public Connection getConnection() throws SQLException {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = "jdbc:mariadb://localhost:3306/exposition?useUnicode=true&characterEncoding=utf-8;";
        String username="aleksandra";
        String password="aleksandra";
        Connection connection = DriverManager
                .getConnection(url, username, password);
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        connection.setAutoCommit(true);
        return connection;
    }
}