package com.epam.rd.summer2020.db;

import com.epam.rd.summer2020.db.entity.UserDTO;

public enum Role {
    ADMIN, LOGIN_USER;

    public static Role getRole(UserDTO user) {
        int roleId = user.getRoleId();
        return Role.values()[roleId];
    }

    public String getName() {
        return name().toLowerCase();
    }
}
