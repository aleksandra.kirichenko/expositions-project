package com.epam.rd.summer2020.db;
import com.epam.rd.summer2020.db.entity.EntityMapper;
import com.epam.rd.summer2020.db.entity.PictureDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PictureDAO {

    private static final String SQL__ADD_PICTURE =
            "INSERT INTO picture (name, author_id, exposition_id) VALUES (?,?,?)";
    private static final String SQL__GET_ALL_PICTURES =
            "SELECT id, name, author_id, exposition_id FROM picture";
    private static final String SQL__GET_PicturesByExpositionId = "SELECT hall.id, hall.name\n" +
            "FROM exposition\n" +
            "         JOIN hall_exposition ON exposition.id = hall_exposition.exposiition_id\n" +
            "         JOIN hall  on hall_exposition.hall_id = hall.id\n" +
            "WHERE exposition.id = 1;";

    PictureMapper mapper = new PictureMapper();


    public List<PictureDTO> getPicturesByExpositionId(long expositionId) {
        List<PictureDTO> pictureDTOs = new ArrayList<>();
        try (Connection con = DBManager.getInstance().getConnection(); PreparedStatement stat = con.prepareStatement(SQL__GET_PicturesByExpositionId)) {
            stat.setLong(1, expositionId);
            try (ResultSet result = stat.executeQuery()) {
                if (result.first()) {
                    PictureDTO pictureDTO = mapper.mapRow(result);
                    pictureDTOs.add(pictureDTO);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return pictureDTOs;
    }

    public List<PictureDTO> getAllPictures() {
        List<PictureDTO> pictureDTOS = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PictureMapper mapper = new PictureMapper();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL__GET_ALL_PICTURES);
            while (rs.next())
                pictureDTOS.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return pictureDTOS;
    }

    public void addPicture(PictureDTO pictureDTO) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement stat = con.prepareStatement(SQL__ADD_PICTURE);
            stat.setString(1, pictureDTO.getName());
            stat.setInt(2, pictureDTO.getAuthorId());
            stat.setInt(3, pictureDTO.getExpositionId());
            stat.execute();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

    private static class PictureMapper implements EntityMapper<PictureDTO> {
        @Override
        public PictureDTO mapRow(ResultSet rs) {
            try {
                PictureDTO pictureDTO = new PictureDTO();
                pictureDTO.setName(rs.getString(Fields.PICTURE__NAME));
                pictureDTO.setAuthorId(rs.getInt(Fields.PICTURE__AUTHOR_ID));
                pictureDTO.setExpositionId(rs.getInt(Fields.PICTURE__EXPOSITION_ID));
                return pictureDTO;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
