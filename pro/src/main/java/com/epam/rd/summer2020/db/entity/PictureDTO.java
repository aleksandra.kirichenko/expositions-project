package com.epam.rd.summer2020.db.entity;

public class PictureDTO {
    String name;
    int authorId;
    int expositionId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getExpositionId() {
        return expositionId;
    }

    public void setExpositionId(int expositionId) {
        this.expositionId = expositionId;
    }

    @Override
    public String toString() {
        return "PictureDTO{" +
                "name='" + name + '\'' +
                ", authorId=" + authorId +
                ", expositionId=" + expositionId +
                '}';
    }
}
