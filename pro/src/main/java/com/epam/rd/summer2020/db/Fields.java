package com.epam.rd.summer2020.db;

public final class Fields {
    public static final String ENTITY__ID = "id";

    public static final String EXPOSITION__TOPIC = "topic";
    public static final String EXPOSITION__DATE_START  = "date_start";
    public static final String EXPOSITION__DATE_END  = "date_end";
    public static final String EXPOSITION__COST  = "cost";
    public static final String EXPOSITION__NUM_OF_TICKETS  = "num_of_tickets";

    public static final String USER__LOGIN = "login";
    public static final String USER__PASSWORD  = "password";
    public static final String USER__ROLE_ID  = "role_id";

    public static final String PICTURE__NAME  = "name";
    public static final String PICTURE__AUTHOR_ID  = "author_id";
    public static final String PICTURE__EXPOSITION_ID = "exposition_id";

    public static final String AUTHOR__SURNAME  = "surname";
    public static final String AUTHOR__NAME  = "name";

    public static final String HALL__NAME  = "name";


}
