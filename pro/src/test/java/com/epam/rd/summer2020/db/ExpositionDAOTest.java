package com.epam.rd.summer2020.db;

import com.epam.rd.summer2020.db.entity.ExpositionDTO;
import org.junit.Test;

import java.sql.Date;


import static com.epam.rd.summer2020.utils.DateUtils.parseDate;
import static com.epam.rd.summer2020.utils.DateUtils.toSqlDate;


public class ExpositionDAOTest {
    ExpositionDAO expositionDAO = new ExpositionDAO();


    @Test
    public void getAllExpositions() {
        System.out.println(expositionDAO.getAllExpositions());
    }

    @Test
    public void filterByDate() {
        Date from = toSqlDate(parseDate("2020-11-01"));
        Date till =toSqlDate(parseDate("2020-11-12"));
        System.out.println(expositionDAO.filterByDate(from, till));
    }

    @Test
    public void addNewExposition() {
        ExpositionDTO expositionDTO = new ExpositionDTO();
        expositionDTO.setTopic("Waterfall");
        expositionDTO.setDateStart(toSqlDate(parseDate("2021-01-01")));
        expositionDTO.setDateEnd(toSqlDate(parseDate("2021-02-22")));
        expositionDTO.setCost(300);
        expositionDTO.setNumOfTickets(1000);
        expositionDAO.addNewExposition(expositionDTO);
    }

    @Test
    public void getExpositionById(){
        long id = 8;
        System.out.println(expositionDAO.getExpositionById(11));
    }

    @Test
    public void  deleteExpositionByTopic(){
        String topic = "Winter time pro";
        expositionDAO.deleteExpositinByTopic(topic);
    }

}
